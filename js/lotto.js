﻿//Matt Hoing
//SSD Yr1, Sem2
//Client Side Programming CA assignment 1 - Lotto

var myNumbers = new Array();
btns = new Array();

playGame=document.getElementById("play");
playGame.addEventListener("click", newNumbers, checkNums, false);//enables the play game button

//function to take input from the user and put them into an array called myNumbers
function getNumbers(){
myNumbers[0]=parseInt(document.getElementById("num1").value);
myNumbers[1]=parseInt(document.getElementById("num2").value);
myNumbers[2]=parseInt(document.getElementById("num3").value);
myNumbers[3]=parseInt(document.getElementById("num4").value);
myNumbers[4]=parseInt(document.getElementById("num5").value);
myNumbers[5]=parseInt(document.getElementById("num6").value);
}

function validatenumber(num) {
    thevalue=num.value;
    if (thevalue.length==0 || isNaN(thevalue) || thevalue<1 ||  thevalue>45) {
        alert("Invalid number entry.. Please enter a valid number");
        num.focus();
        num.value="";
    } else {myNumbers.push(thevalue);}
}

//check user input is between 1 and 45
 var fieldOne = document.getElementById("num1");
 var fieldTwo = document.getElementById("num2");
 var fieldThree = document.getElementById("num3");
 var fieldFour = document.getElementById("num4");
 var fieldFive = document.getElementById("num5");
 var fieldSix = document.getElementById("num6");

 fieldOne.addEventListener("change", function() {validatenumber(fieldOne);}, false);
 fieldTwo.addEventListener("change", function() {validatenumber(fieldTwo);}, false);
 fieldThree.addEventListener("change", function() {validatenumber(fieldThree);}, false);
 fieldFour.addEventListener("change", function() {validatenumber(fieldFour);}, false);
 fieldFive.addEventListener("change", function() {validatenumber(fieldFive);}, false);
 fieldSix.addEventListener("change", function() {validatenumber(fieldSix);}, false);




function checkNums(Lotto, num) {
//This function searches an array(Lotto) for the existence of a number(num).
//If it is found, true is returned else false is returned.
 
          x=Lotto.indexOf(num);
           if (x!=-1) {;
           return true;
       }
  return false;
}



function newNumbers() {
//This function is to be called when the button Play Game is clicked.
//It generates 6 random (unique) numbers between 1 and 45.
getNumbers();
if (myNumbers.length!=6) {
	alert("You must select your own 6 numbers first")
}
else {
	lottoNumbers = new Array();
	for (i=0; i<=5; i++) {
		newnum=Math.floor(Math.random()*45+1);
		if (checkNums(lottoNumbers, newnum)) {
			i--;
    }
    else lottoNumbers.push(newnum);
}
	document.getElementById("numbers").value = lottoNumbers.sort(function(a,b){return a - b});//output the lotto numbers sorted and assign "numbers" box
	document.getElementById("mynumbers").value = myNumbers.sort(function(a,b){return a - b}); //output your  numbers sorted and assign "mynumbers" box
	goCompare(); 
}
}


//call a function to compare the numbers and display the result
function goCompare(){
  snap = 0;
for(i = 0; i < myNumbers.length; i++){
for(j = 0; j < lottoNumbers.length; j++){
if(myNumbers[i] == lottoNumbers[j]){
snap++;
break; 
}
 }
}


//put matching result into results box

switch(snap){
case 1:
document.getElementById("results").value = snap + " number matched " +  " you win €5!";
break;
case 2:
document.getElementById("results").value = snap + " numbers matched " + " you win €10!";
break;
case 3:
document.getElementById("results").value = snap + " numbers matched " + " you win €15, congratulations";
break;
case 4:
document.getElementById("results").value = snap + " numbers matched " + " you win €59, congratulations";
break;
case 5:
document.getElementById("results").value = snap + " numbers matched " + " you win €1,742, congratulations";
break;
case 6:
document.getElementById("results").value = snap + " numbers matched " + " you win €4,907,803 congratulations! I'd like a 4 bedroomed detatched house please";
break;
default:
}
}




//Jackpot Winner	0	€4,907,803
//Match 5 + Bonus	1	€25,000
//Match 5	39	€1,742
//Match 4 + Bonus	101	€169
//Match 4	1,773	€59
//Match 3 + Bonus	2,502	€28
//Match 3	28,850	€5
//28-02-2015 prizes
//IGNORE LOTTO PRIZE FUND - MAKE YOUR OWN!!!
//to sort numbers into ascending code: document.write(myNumbers.sort(function(a,b){return a - b}));

 function formatCurrency(num) {
//This function takes in a number(num) and returns it in edited form with an euro symbol and commas if necessary.
 
  num = num.toString().replace(/\€|\,/g,'');
  if(isNaN(num))
   {num = "0";}
  sign = (num == (num = Math.abs(num)));
  num = num.toFixed(2);
  elements= num.split(".");
  num = elements[0];
  cents = elements[1];
  for (var i = 0; i < Math.floor((num.length-(1+i))/3); i++)
   {num = num.substring(0,num.length-(4*i+3))+','+
    num.substring(num.length-(4*0+3));}
  return (((sign)?'':'-') + '€' + num + '.' + cents)
}
